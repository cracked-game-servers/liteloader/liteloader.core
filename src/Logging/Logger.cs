﻿using LiteLoader.DependencyInjection;
using System;

namespace LiteLoader.Logging
{
    internal abstract class Logger : SubscriptionHandler<ILogWriter>, ILogger, IDisposable
    {
        public DebugCallback DebugCallback { get; internal set; }

        protected int SubLoggers
        {
            get
            {
                lock (Subscriptions)
                {
                    return Subscriptions.Count;
                }
            }
        }

        public void LogMessage(object message, string prefix = null, LogMessage.Level priority = Logging.LogMessage.Level.Information)
        {
#if !DEBUG
            if (priority == Logging.LogMessage.Level.Debug)
            {
                return;
            }
#endif
            try
            {
                LogMessage log = Logging.LogMessage.Create(message, prefix, priority);
                OnLogMessage(log);
                DebugCallback?.Invoke(log);
            }
            catch (Exception)
            {
            }
        }

        protected abstract void OnLogMessage(LogMessage message);

        protected void WriteToWriters(LogMessage message)
        {
            lock (Subscriptions)
            {
                foreach (ILogWriter writer in Subscriptions)
                {
                    writer.Write(message);
                }
            }
        }

        #region IDisposable Support

        private bool disposedValue;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                disposedValue = true;

                if (disposing)
                {
                    Subscriptions.Clear();
                }
            }
        }

        ~Logger()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
