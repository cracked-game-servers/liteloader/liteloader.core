﻿using System;

namespace LiteLoader.Plugins.Results
{
    internal class PluginLoadResult : IPluginResult
    {
        public Exception Exception { get; private set; }
        public string Message { get; private set; }
        public Plugin Plugin { get; private set; }
        public string PluginName { get; private set; }
        public bool Success { get; private set; }

        private PluginLoadResult()
        {
        }

        public static PluginLoadResult FromFail(string plugin, string message = null, Exception exception = null)
        {
            PluginLoadResult fail = new PluginLoadResult();
            fail.PluginName = plugin;
            fail.Message = message;
            fail.Success = false;
            fail.Exception = exception;
            return fail;
        }

        public static PluginLoadResult FromSuccess(Plugin plugin, string message = null)
        {
            PluginLoadResult success = FromSuccess(plugin.GetType().Name, message);
            success.Plugin = plugin;
            return success;
        }

        public static PluginLoadResult FromSuccess(string plugin, string message = null)
        {
            PluginLoadResult success = new PluginLoadResult();
            success.Success = true;
            success.PluginName = plugin.GetType().Name;
            success.Message = message;
            return success;
        }
    }
}
