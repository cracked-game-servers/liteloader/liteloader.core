﻿using System;
using System.IO;
using System.Reflection;

namespace LiteLoader.Extensions
{
    internal static class ExtensionUtility
    {
        internal static Assembly LoadAssembly(string filename, bool ignoreSymbols = false, bool reflectionOnly = false)
        {
            if (string.IsNullOrEmpty(filename))
            {
                throw new ArgumentNullException(nameof(filename));
            }

            if (!File.Exists(filename))
            {
                throw new IOException($"{filename} does not exist");
            }

            byte[] assembly = File.ReadAllBytes(filename);

            if (ignoreSymbols || reflectionOnly)
            {
                return reflectionOnly ? Assembly.ReflectionOnlyLoad(assembly) : Assembly.Load(assembly);
            }

            string pdbFile = Path.ChangeExtension(filename, ".pdb");

            if (!File.Exists(pdbFile))
            {
                return Assembly.Load(assembly);
            }

            byte[] pdb = File.ReadAllBytes(pdbFile);
            return Assembly.Load(assembly, pdb);
        }
    }
}
