﻿using LiteLoader.Utilities;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;

namespace LiteLoader.Logging
{
    internal class ThreadedLogger : Logger
    {
        private readonly Queue<LogMessage> logQueue;
        private readonly AutoResetEvent resetEvent;
        private readonly Thread workerThread;

        private bool disposedValue;

        public ThreadedLogger()
        {
            Subscriptions.Add(new FileLogger());
            logQueue = new Queue<LogMessage>();
            resetEvent = new AutoResetEvent(false);
            workerThread = new Thread(HandleWork)
            {
                IsBackground = true,
                Name = "LiteLoader.ThreadedLogger"
            };
            workerThread.Start();
        }

        protected override void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                disposedValue = true;
                resetEvent.Set();
                workerThread.Join();
                resetEvent.Close();

                if (disposing)
                {
                    lock (logQueue)
                    {
                        logQueue.Clear();
                    }
                }
            }

            base.Dispose(disposing);
        }

        protected override void OnLogMessage(LogMessage message)
        {
            if (disposedValue)
            {
                throw new ObjectDisposedException(GetType().FullName);
            }

            lock (logQueue)
            {
                logQueue.Enqueue(message);
            }

            resetEvent.Set();
        }

        private void HandleWork()
        {
            while (!disposedValue)
            {
                resetEvent.WaitOne();

                if (SubLoggers == 0)
                {
                    continue;
                }

                NotifyLoggers(false);
                while (true)
                {
                    lock (logQueue)
                    {
                        if (logQueue.Count == 0)
                        {
                            break;
                        }

                        WriteToWriters(logQueue.Dequeue());
                    }
                }
                NotifyLoggers(true);
            }
        }

        private void NotifyLoggers(bool close)
        {
            lock (Subscriptions)
            {
                foreach (var sub in Subscriptions)
                {
                    MethodInfo method = sub.GetType().GetMethod(!close ? "BeginBatch" : "EndBatch", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                    if (method != null)
                    {
                        try
                        {
                            method.Invoke(sub, ArrayPool.Get<object>(0));
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }
        }
    }
}
