﻿using LiteLoader.Utilities;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace LiteLoader.DependencyInjection
{
    internal class DynamicServiceProvider : IDynamicServiceCollection
    {
        private readonly List<ServiceDescriptor> services;
        private IInvokeManager activationUtility;

        internal DynamicServiceProvider()
        {
            services = new List<ServiceDescriptor>()
            {
                ServiceDescriptor.CreateSingleton<IServiceProvider>(this)
            };
        }

        public void Add(ServiceDescriptor descriptor)
        {
            if (descriptor == null)
            {
                throw new ArgumentNullException(nameof(descriptor));
            }

            if (Contains(descriptor.ServiceType))
            {
                throw new InvalidOperationException($"Service {descriptor.ServiceType.FullName} already exists");
            }

            lock (services)
            {
                services.Add(descriptor);
            }

            var extensionLoader = GetService(typeof(Extensions.IExtensionLoader)) as Extensions.ExtensionLoader;

            if (extensionLoader != null)
            {
                extensionLoader.DisposeOnUnload(descriptor.InstanceType.Assembly, new CallbackDisposable(() =>
                {
                    lock (services)
                    {
                        services.Remove(descriptor);
                    }
                }));
            }
        }

        public bool Contains(Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            if (type == typeof(IServiceProvider))
            {
                return true;
            }

            bool match = false;
            lock (services)
            {
                for (int i = 0; i < services.Count; i++)
                {
                    if (services[i].ServiceType == type)
                    {
                        return true;
                    }

                    if (services[i].ServiceType.IsAssignableFrom(type))
                    {
                        match = true;
                    }
                }
            }

            return match;
        }

        public object GetService(Type serviceType)
        {
            if (serviceType == null)
            {
                return null;
            }

            if (serviceType.IsByRef)
            {
                serviceType = serviceType.GetElementType();
            }

            if (serviceType == typeof(IServiceProvider))
            {
                return this;
            }

            ServiceDescriptor descriptor = null;
            lock (services)
            {
                for (int i = 0; i < services.Count; i++)
                {
                    ServiceDescriptor serviceDescriptor = services[i];
                    if (serviceDescriptor.InstanceType.IsAssignableFrom(serviceType))
                    {
                        descriptor = serviceDescriptor;
                        break;
                    }

                    if (serviceDescriptor.ServiceType.IsAssignableFrom(serviceType))
                    {
                        descriptor = serviceDescriptor;
                    }
                }
            }

            if (descriptor == null)
            {
                return null;
            }

            object[] context = null;
            int?[] map = null;
            if (descriptor.IsTransient)
            {
                try
                {
                    var constructors = descriptor.InstanceType.GetConstructors(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                    if (!activationUtility.TryFindBestMatch(constructors, ArrayPool.Get<object>(0), out var best, out context, out map))
                    {
                        throw new InvalidOperationException($"Unable to find valid constructor for {descriptor.InstanceType}");
                    }

                    return activationUtility.Invoke(best, context);
                }
                finally
                {
                    ArrayPool.Free(context);
                    ArrayPool.Free(map);
                }
            }

            if (descriptor.Instance != null)
            {
                return descriptor.Instance;
            }

            try
            {
                var constructors = descriptor.InstanceType.GetConstructors(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                if (!activationUtility.TryFindBestMatch(constructors, ArrayPool.Get<object>(0), out var best, out context, out map))
                {
                    throw new InvalidOperationException($"Unable to find valid constructor for {descriptor.InstanceType}");
                }

                descriptor.Instance = activationUtility.Invoke(best, context);
                return descriptor.Instance;
            }
            finally
            {
                ArrayPool.Free(context);
                ArrayPool.Free(map);
            }
        }

        internal void SetActivationContext(IInvokeManager utility)
        {
            activationUtility = utility;
        }
    }
}
