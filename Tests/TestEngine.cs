﻿using System;
using System.Threading.Tasks;

namespace LiteLoader.Core.Tests
{
    internal class TestEngine : Game.IEngine
    {
        public string Engine => "Test Engine";

        public float EngineTime => Environment.TickCount;

        public void Delay(TimeSpan timeSpan, Action callback)
        {
            Task.Delay(timeSpan).ContinueWith((task) => callback());
        }

        public IDisposable Every(TimeSpan timeSpan, Action callback)
        {
            throw new NotImplementedException();
        }

        public void NextUpdate(Action callback)
        {
            Task.Factory.StartNew(callback);
        }
    }
}
