﻿using LiteLoader.DependencyInjection;
using LiteLoader.Plugins.Results;
using LiteLoader.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace LiteLoader.Plugins
{
    internal sealed class RootPluginHandler : IPluginHandler
    {
        private readonly IInvokeManager activator;
        private readonly List<PluginLoader> pluginLoaders;
        private Assembly initialAssembly = null;

        public RootPluginHandler()
        {
            pluginLoaders = new List<PluginLoader>();
            activator = Interface.LiteLoader.ServiceProvider.GetService<IInvokeManager>();
        }

        public Plugin GetPlugin(string plugin)
        {
            lock (pluginLoaders)
            {
                for (int i = 0; i < pluginLoaders.Count; i++)
                {
                    PluginLoader loader = pluginLoaders[i];
                    Plugin plg = loader.GetPlugin(plugin);

                    if (plg == null) continue;
                    return plg;
                }
            }

            return null;
        }

        public IEnumerable<string> GetPlugins()
        {
            lock (pluginLoaders)
            {
                return pluginLoaders
                    .SelectMany(e => e.ScanForPlugins())
                    .Distinct()
                    .OrderByDescending(e => e);
            }
        }

        public object InvokeAll(string hookName, object[] arguments)
        {
            object[] responses = null;
            bool isInternalCall = hookName[0].Equals('I');

            lock (pluginLoaders)
            {
                responses = ArrayPool.Get<object>(pluginLoaders.Count);

                for (int i = 0; i < pluginLoaders.Count; i++)
                {
                    PluginLoader loader = pluginLoaders[i];

                    if (isInternalCall && loader.GetType().Assembly != initialAssembly)
                    {
                        responses[i] = null;
                        continue;
                    }

                    responses[i] = loader.InvokePlugins(hookName, arguments);
                }
            }

            for (int i = 0; i < responses.Length; i++)
            {
                object value = responses[i];
                if (value == null || value == DBNull.Value)
                {
                    continue;
                }

                if (value != null)
                {
                    ArrayPool.Free(responses);
                    return value;
                }
            }
            ArrayPool.Free(responses);
            return null;
        }

        public IPluginResult LoadPlugin(string plugin)
        {
            if (string.IsNullOrEmpty(plugin))
            {
                throw new ArgumentNullException(nameof(plugin));
            }

            lock (pluginLoaders)
            {
                for (int i = 0; i < pluginLoaders.Count; i++)
                {
                    PluginLoader loader = pluginLoaders[i];
                    if (!loader.HasPlugin(plugin)) continue;

                    if (loader.IsPluginLoaded(plugin))
                    {
                        return PluginLoadResult.FromSuccess(loader.GetPlugin(plugin), "Plugin is already loaded");
                    }

                    if (loader.IsPluginLoading(plugin))
                    {
                        return PluginLoadResult.FromSuccess(plugin, "Plugin is currently loading");
                    }

                    Plugin p = loader.Load(plugin);

                    if (p != null)
                    {
                        return PluginLoadResult.FromSuccess(p);
                    }
                    else
                    {
                        if (loader.IsPluginLoading(plugin))
                        {
                            return PluginLoadResult.FromSuccess(plugin, "Plugin is loading");
                        }

                        return PluginLoadResult.FromFail(plugin, "Loader returned unknown state");
                    }
                }
            }

            return PluginLoadResult.FromFail(plugin, "Unable to find suitable plugin loader");
        }

        public void OnExtensionLoaded(Assembly extension)
        {
            Type[] types = extension.GetTypes();
            Type loaderType = typeof(PluginLoader);
            for (int i = 0; i < types.Length; i++)
            {
                Type type = types[i];

                if (type.IsAbstract || type.IsInterface || !loaderType.IsAssignableFrom(type)) continue;

                lock (pluginLoaders)
                {
                    bool found = false;
                    for (int x = 0; x < pluginLoaders.Count; x++)
                    {
                        if (pluginLoaders[x].GetType() == type)
                        {
                            found = true;
                            break;
                        }
                    }

                    if (found)
                    {
                        continue;
                    }

                    try
                    {
                        var constructors = type.GetConstructors(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
                        if (!activator.TryFindBestMatch(constructors, ArrayPool.Get<object>(0), out var best, out var context, out var map))
                        {
                            continue;
                        }

                        var loader = (PluginLoader)activator.Invoke(best, context);

                        if (loader != null)
                        {
                            pluginLoaders.Add(loader);

                            if (initialAssembly == null)
                            {
                                initialAssembly = extension;
                            }

                            loader.LoadAll();
                        }
                    }
                    catch (Exception e)
                    {
                        Interface.LiteLoader.ServiceProvider.GetService<Logging.ILogger>().LogMessage(e, "PluginLoader", Logging.LogMessage.Level.Stacktrace);
                    }
                }
            }
        }

        public void OnExtensionUnloaded(Assembly extension)
        {
            lock (pluginLoaders)
            {
                PluginLoader[] loaders = pluginLoaders.Where(l => l.GetType().Assembly == extension).ToArray();
                foreach (PluginLoader loader in loaders)
                {
                    loader.UnloadAll();
                    pluginLoaders.Remove(loader);
                }
            }
        }

        public IPluginResult ReloadPlugin(string plugin)
        {
            if (string.IsNullOrEmpty(plugin))
            {
                throw new ArgumentNullException(nameof(plugin));
            }

            lock (pluginLoaders)
            {
                for (int i = 0; i < pluginLoaders.Count; i++)
                {
                    PluginLoader loader = pluginLoaders[i];
                    if (loader.HasPlugin(plugin))
                    {
                        if (loader.IsPluginLoaded(plugin))
                        {
                            if (loader.Reload(plugin))
                            {
                                return PluginLoadResult.FromSuccess(plugin, "Plugin queued for reload");
                            }
                        }
                        break;
                    }
                }
            }

            return PluginLoadResult.FromFail(plugin, "Unable to find a loaded plugin");
        }

        public void UnloadAllPlugins()
        {
            lock (pluginLoaders)
            {
                for (int i = 0; i < pluginLoaders.Count; i++)
                {
                    pluginLoaders[i].UnloadAll();
                }
            }
        }

        public IPluginResult UnloadPlugin(string plugin)
        {
            if (string.IsNullOrEmpty(plugin))
            {
                throw new ArgumentNullException(nameof(plugin));
            }

            lock (pluginLoaders)
            {
                for (int i = 0; i < pluginLoaders.Count; i++)
                {
                    PluginLoader loader = pluginLoaders[i];

                    if (loader.HasPlugin(plugin))
                    {
                        if (loader.IsPluginLoaded(plugin))
                        {
                            loader.Unload(plugin);
                            return PluginLoadResult.FromSuccess(plugin);
                        }
                        return PluginLoadResult.FromSuccess(plugin, "Already unloaded");
                    }
                }
            }

            return PluginLoadResult.FromFail(plugin, "Unable to find suitable loader");
        }
    }
}
