﻿using LiteLoader.Plugins;
using System;
using System.Threading.Tasks;

namespace LiteLoader.Core.Tests
{
    [Plugin("Test Plugin", "KahunaElGrande", "1.0.0")]
    public class TestPlugin : ReflectionPlugin
    {
        private Action TestCallback;

        public Task BasicAsyncHook()
        {
            return new Task(() =>
            {
                TestCallback();
            });
        }

        public void BasicHook()
        {
            TestCallback();
        }

        public string ParamConversionHook(string longDate)
        {
            TestCallback();
            return longDate;
        }

        public Task<string> ReturnAsyncHook()
        {
            return new Task<string>(() =>
            {
                TestCallback();
                return "HelloWorldAsync";
            });
        }

        public Task<Object> ReturnAsyncParamHook(Object value)
        {
            return new Task<Object>(() =>
            {
                TestCallback();
                return value;
            });
        }

        public string ReturnHook()
        {
            TestCallback();
            return "HelloWorld";
        }

        public Object ReturnParamHook(Object value)
        {
            TestCallback();
            return value;
        }

        public void TestInit(Action testCallback)
        {
            TestCallback = testCallback;
        }
    }
}
