﻿using LiteLoader.Plugins;
using System;

namespace LiteLoader.Core.Tests
{
    internal class TestPluginLoader : PluginLoader
    {
        public TestPluginLoader()
        {
            Logger.LogMessage("PluginLoader Initialized");
        }

        public override void LoadAll()
        {
            Logger.LogMessage("LoadAll Called");
            foreach (var plugin in ScanForPlugins()) Logger.LogMessage(plugin);
            base.LoadAll();
        }

        protected override Plugin InstantiatePlugin(string pluginName)
        {
            Logger.LogMessage("Loading Plugin " + pluginName);
            try
            {
                var p = base.InstantiatePlugin(pluginName);
                Logger.LogMessage(p == null ? "Null Plugin" : $"Loaded Plugin {p.GetType().FullName}");
                return p;
            }
            catch (Exception e)
            {
                Logger.LogMessage(e);
            }
            return null;
        }
    }
}
