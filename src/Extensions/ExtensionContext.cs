﻿using System;
using System.Collections.Generic;

namespace LiteLoader.Extensions
{
    internal class ExtensionContext : IExtensionContainer
    {
        public string Author { get; set; }
        public string Checksum { get; }

        public bool IsLoaded { get; set; }
        public string Location { get; set; }
        public string Name { get; set; }

        public List<IDisposable> OnUnloadDisposables { get; }
        public Type Startup { get; set; }
        public string Title { get; set; }
        public Version Version { get; set; }

        public ExtensionContext(string checksum)
        {
            Checksum = checksum;
            OnUnloadDisposables = new List<IDisposable>();
        }
    }
}
