﻿using System;
using System.IO;
using System.Text;

namespace LiteLoader.Logging
{
    internal class FileLogger : ILogWriter
    {
        private StreamWriter writer = null;

        public void Write(LogMessage message)
        {
            writer.WriteLine(message.ToString(true));
        }

        private void BeginBatch()
        {
            if (writer == null)
            {
                string fileName = Interface.LiteLoader.LoggingDirectory;
                
                if (!Directory.Exists(fileName)) Directory.CreateDirectory(fileName);
                DateTime now = DateTime.Now;
                string name = $"LiteLoader_{now.Year}.{now.Month}.{now.Day}.log";
                fileName = Path.Combine(fileName, name);
                writer = new StreamWriter(File.Open(fileName, FileMode.Append, FileAccess.Write, FileShare.Read), Encoding.UTF8);
            }
        }

        private void EndBatch()
        {
            if (writer != null)
            {
                writer.Flush();
                writer.Close();
                writer.Dispose();
                writer = null;
            }
        }
    }
}
