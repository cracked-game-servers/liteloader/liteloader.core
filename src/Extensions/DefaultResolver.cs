﻿using System;
using System.Reflection;

namespace LiteLoader.Extensions
{
    internal static class DefaultResolver
    {
        public static ExtensionLoader ExtensionLoader { get; internal set; }

        public static Assembly Resolve(object source, ResolveEventArgs name)
        {
            try
            {
                return Resolve(source != null && source is Assembly assembly ? assembly : null, new AssemblyName(name.Name));
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static Assembly Resolve(Assembly caller, AssemblyName name)
        {
            if (name == null)
            {
                return null;
            }

            // Basic Resolve
            Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();

            if (assemblies == null || assemblies.Length == 0)
            {
                return null;
            }

            for (int i = 0; i < assemblies.Length; i++)
            {
                Assembly assembly = assemblies[i];

                if (assembly.FullName.Equals(name.FullName, StringComparison.Ordinal))
                {
                    return assembly;
                }
            }

            return null;
        }
    }
}
