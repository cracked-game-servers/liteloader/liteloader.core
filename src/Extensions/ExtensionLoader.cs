﻿using LiteLoader.DependencyInjection;
using LiteLoader.Logging;
using LiteLoader.Plugins;
using LiteLoader.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

namespace LiteLoader.Extensions
{
    internal sealed class ExtensionLoader : IExtensionLoader
    {
        private readonly IInvokeManager activationUtility;
        private readonly Dictionary<Assembly, ExtensionContext> loadedContexts;
        private readonly ThreadedLogger logger;
        private readonly RootPluginHandler pluginHandler;
        public string ExtensionsPath { get; }
        internal Assembly StartupExtension { get; private set; }

        public ExtensionLoader(IInvokeManager utility, string directory, RootPluginHandler loader, ThreadedLogger logger)
        {
            activationUtility = utility;
            ExtensionsPath = directory;
            pluginHandler = loader;
            DefaultResolver.ExtensionLoader = this;
            this.logger = logger;
            loadedContexts = new Dictionary<Assembly, ExtensionContext>(1)
            {
                [GetType().Assembly] = new ExtensionContext(null) { Name = "LiteLoader.Core", Title = "Core", Author = "KahunaElGrande", Version = GetType().Assembly.GetName().Version }
            };
        }

        #region Extension Management

        public bool IsLoaded(string extName)
        {
            if (string.IsNullOrEmpty(extName))
            {
                return false;
            }

            if (!extName.StartsWith("LiteLoader.", StringComparison.InvariantCultureIgnoreCase))
            {
                extName = "LiteLoader." + extName;
            }

            lock (loadedContexts)
            {
                foreach (var kv in loadedContexts)
                {
                    if (!kv.Value.Name.Equals(extName, StringComparison.InvariantCultureIgnoreCase))
                    {
                        continue;
                    }

                    if (!kv.Value.IsLoaded)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public IExtensionResult Load(string extName)
        {
            if (string.IsNullOrEmpty(extName))
            {
                return ExtensionLoadResult.Fail("No path provided");
            }

            if (!File.Exists(extName))
            {
                extName = GetExtensionPath(extName);

                if (!File.Exists(extName))
                {
                    return ExtensionLoadResult.Fail($"Unable to find extension at path '{extName}'");
                }
            }

            byte[] lib;
            byte[] pdb = null;

            try
            {
                lib = File.ReadAllBytes(extName);
            }
#pragma warning disable CA1031 // Do not catch general exception types
            catch (Exception e)
            {
                return ExtensionLoadResult.Fail(e);
            }
#pragma warning restore CA1031 // Do not catch general exception types

            string symbols = Path.ChangeExtension(extName, ".pdb");

            if (File.Exists(symbols))
            {
                try
                {
                    pdb = File.ReadAllBytes(symbols);
                }
#pragma warning disable CA1031 // Do not catch general exception types
                catch (Exception)
                {
                    // TODO: Debug Log
                }
#pragma warning restore CA1031 // Do not catch general exception types
            }

            return Load(lib, pdb);
        }

        public IExtensionResult Load(byte[] libraryData, byte[] symbolsData)
        {
            if (libraryData == null || libraryData.Length == 0)
            {
                return ExtensionLoadResult.Fail("No data provided");
            }

            string hash = CryptographyHelper.ComputeMD5(libraryData);
            ExtensionContext context = null;
            Assembly assembly = null;
            lock (loadedContexts)
            {
                foreach (var kv in loadedContexts)
                {
                    if (kv.Value.Checksum == null)
                    {
                        continue;
                    }

                    if (kv.Value.Checksum.Equals(hash, StringComparison.Ordinal))
                    {
                        context = kv.Value;
                        assembly = kv.Key;
                        if (kv.Value.IsLoaded) return ExtensionLoadResult.Successful(context, "Already loaded");
                    }
                }

                if (context == null && assembly == null)
                {
                    context = new ExtensionContext(hash);

                    if (!TryReadRawData(libraryData, symbolsData, out assembly, false))
                    {
                        return ExtensionLoadResult.Fail("Failed to read library data");
                    }

                    loadedContexts[assembly] = context;
                    var attribute = ExtensionInfoAttribute.Get(assembly);
                    context.Author = attribute.Author;
                    context.Name = assembly.GetName().Name;
                    context.Title = attribute.Title;
                    context.Startup = attribute.Startup;
                    context.IsLoaded = false;
                    context.Version = assembly.GetName().Version;
                }

                if (StartupExtension == null)
                {
                    StartupExtension = assembly;
                }
            }

            string path = Path.GetDirectoryName(GetExtensionPath(context.Name));
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            var result = CallExtensionMethod(context.Startup, "ConfigureServices", new object[] { Interface.LiteLoader.ServiceProvider }, true);

            if (result != null)
            {
                if (result is IDisposable disposable)
                {
                    context.OnUnloadDisposables.Add(disposable);
                }
                else if (result is IEnumerable<IDisposable> disposables)
                {
                    context.OnUnloadDisposables.AddRange(disposables);
                }
            }

            CallExtensionMethod(context.Startup, "LoadExtension", new object[] { context });
            context.IsLoaded = true;
            pluginHandler.OnExtensionLoaded(assembly);
            return ExtensionLoadResult.Successful(context);
        }

        public IExtensionResult Unload(string extName)
        {
            if (string.IsNullOrEmpty(extName))
            {
                return ExtensionLoadResult.Fail("No name provided");
            }

            lock (loadedContexts)
            {
                var c = loadedContexts.FirstOrDefault(kv => kv.Value.Name.Equals(extName, StringComparison.OrdinalIgnoreCase));
                
                if (c.Key == null)
                {
                    return ExtensionLoadResult.Fail("Unable to find valid extension");
                }

                if (c.Key == StartupExtension)
                {
                    return ExtensionLoadResult.Fail("Can't unload game extension");
                }

                pluginHandler.OnExtensionUnloaded(c.Key);
                for (int i = 0; i < c.Value.OnUnloadDisposables.Count; i++)
                {
                    IDisposable d = c.Value.OnUnloadDisposables[i];
                    d.Dispose();
                }
                c.Value.OnUnloadDisposables.Clear();
                CallExtensionMethod(c.Value.Startup, "UnloadExtension");
                c.Value.IsLoaded = false;
                return ExtensionLoadResult.Successful(c.Value);
            }
        }

        #endregion

        public object CallExtensionMethod(Type type, string methodName, object[] parameters = null, bool throwException = false)
        {
            if (type == null || string.IsNullOrEmpty(methodName))
            {
                return null;
            }

            if (parameters == null) parameters = ArrayPool.Get<object>(0);

            MethodInfo[] methods = type.GetMethods(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static)
                .Where(m => m.Name.Equals(methodName, StringComparison.OrdinalIgnoreCase)).ToArray();

            if (methods.Length == 0)
            {
                return null;
            }

            if (activationUtility.TryFindBestMatch(methods, parameters, out var best, out var context, out var map))
            {
                try
                {
                    object value = activationUtility.Invoke(best, context);
                    logger.LogMessage($"Called method {best}", "Extensions", LogMessage.Level.Debug);
                    return value;
                }
                catch (Exception e)
                {
                    if (e is TargetInvocationException && e.InnerException != null)
                    {
                        e = e.InnerException;
                    }
                    logger.LogMessage($"Failed to call method {best} | {e.GetType().Name}: {e.Message}", "Extensions", LogMessage.Level.Debug);
                    if (throwException) throw e;
                }
                finally
                {
                    ArrayPool.Free(context);
                    ArrayPool.Free(map);
                }
            }

            return null;
        }

        public bool DisposeOnUnload(Assembly reference, IDisposable disposable)
        {
            if (disposable == null)
            {
                return false;
            }

            ExtensionContext context = null;
            Assembly assembly = reference ?? GetType().Assembly;

            lock (loadedContexts)
            {
                if (!loadedContexts.TryGetValue(assembly, out context))
                {
                    logger.LogMessage($"Unable to register disposable for unregistered Extension '{assembly.FullName}'", "Extensions", LogMessage.Level.Debug);
                    return false;
                }
            }

            lock (context.OnUnloadDisposables)
            {
                if (context.OnUnloadDisposables.Contains(disposable))
                {
                    logger.LogMessage($"Unable to register already existing disposable for Extension '{context.Name}'", "Extensions", LogMessage.Level.Debug);
                    return false;
                }

                context.OnUnloadDisposables.Add(disposable);
                logger.LogMessage($"Registered disposable( {disposable.GetType().FullName} ) for Extension '{context.Name}'", "Extensions", LogMessage.Level.Debug);
            }
            return true;
        }

        public string[] FindExtensions()
        {
            var path = new DirectoryInfo(ExtensionsPath);
            return path.GetDirectories("LiteLoader.*", SearchOption.TopDirectoryOnly).Select(d => d.Name).ToArray();
        }

        public string GetExtensionPath(string extName)
        {
            if (string.IsNullOrEmpty(extName))
            {
                throw new ArgumentNullException(nameof(extName));
            }

            Match match = Regex.Match(extName, @"^LiteLoader\.(?'Name'\S+)$", RegexOptions.IgnoreCase);

            string name = match.Success ? match.Groups["Name"].Value : extName;
            string fullPath = Path.Combine(ExtensionsPath, $"LiteLoader.{name}{Path.DirectorySeparatorChar}LiteLoader.{name}.dll");
            return Path.GetFullPath(fullPath);
        }

        public void InvokeAllExtensions(string methodName, object[] parameters = null)
        {
            lock (loadedContexts)
            {
                foreach (var kv in loadedContexts)
                {
                    CallExtensionMethod(kv.Value.Startup, methodName, parameters);
                }
            }
        }

        public void PreloadFileSystem()
        {
            foreach (string str in FindExtensions())
            {
                logger.LogMessage($"Loading extension file: {str}", "Extensions", LogMessage.Level.Debug);
                var result = Load(str);
                if (result.Success)
                {
                    logger.LogMessage($"Loaded extension {result.Extension.Title} v{result.Extension.Version} by {result.Extension.Author}", "Extensions", LogMessage.Level.Information);
                }
                else
                {
                    logger.LogMessage($"Failed to load extension - {result.Message} {result.Exception}", "Extensions", LogMessage.Level.Error);
                }
            }
        }

        private bool TryReadRawData(byte[] library, byte[] symbols, out Assembly assembly, bool throwException = false)
        {
            assembly = null;
            if (library == null || library.Length == 0)
            {
                return false;
            }

            try
            {
                assembly = symbols != null && symbols.Length > 0 ? Assembly.Load(library, symbols) : Assembly.Load(library);
                logger.LogMessage($"Initialized Assembly {assembly.FullName}", "Extensions", LogMessage.Level.Debug);
                return true;
            }
            catch (Exception e)
            {
                logger.LogMessage($"Failed to read assembly data | {e.GetType().Name}: {e.Message}", "Extensions", LogMessage.Level.Debug);
                if (throwException)
                {
                    throw e;
                }
            }

            return false;
        }
    }
}
