﻿using LiteLoader.Utilities;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace LiteLoader.DependencyInjection
{
    public sealed class InvokeManager : SubscriptionHandler<IParameterBinding>, IInvokeManager
    {
        private readonly Type _voidType;

#if !NET35
        private readonly Type _taskType;
        private readonly Type _genericTaskType;
#endif

        public InvokeManager()
        {
            _voidType = typeof(void);
#if !NET35
            _taskType = typeof(System.Threading.Tasks.Task);
            _genericTaskType = typeof(System.Threading.Tasks.Task<>);
#endif
        }

        public bool TryFindBestMatch(IEnumerable<MethodBase> methods, object[] arguments, out MethodBase best, out object[] context, out int?[] map)
        {
            best = null;
            context = null;
            map = null;
            int bestCount = -1;
            if (arguments == null) arguments = ArrayPool.Get<object>(0);

            foreach (var method in methods)
            {
                var param = method.GetParameters();
                var count = MapParameters(param, arguments, out var c, out var m);

                if (count == -1) continue;

                if (count > bestCount)
                {
                    ArrayPool.Free(context);
                    ArrayPool.Free(map);
                    best = method;
                    context = c;
                    map = m;
                    bestCount = count;
                    continue;
                }

                ArrayPool.Free(c);
                ArrayPool.Free(m);
            }

            return best != null;
        }

        public object Invoke(MethodBase method, object[] orderedArguments = null, object instance = null)
        {
            if (method == null)
            {
                throw new ArgumentNullException(nameof(method));
            }

            if (orderedArguments == null) orderedArguments = ArrayPool.Get<object>(0);

            object value = null;

            if (method is ConstructorInfo constructor)
            {
                value = constructor.Invoke(orderedArguments);
            }
            else
            {
                value = method.Invoke(instance, orderedArguments);
            }

            if (!(method is MethodInfo methodInfo))
            {
                return value;
            }

            if (methodInfo.ReturnType == _voidType)
            {
                return DBNull.Value;
            }

            if (value == null)
            {
                return null;
            }
#if !NET35
            if (_taskType.IsInstanceOfType(value))
            {
                // TODO: Use Attribute to set execution type
                return HandleTaskReturn((System.Threading.Tasks.Task)value, Environment.TickCount);
            }
#endif

            return value;
        }

        public int MapParameters(ParameterInfo[] parameters, object[] args, out object[] context, out int?[] map)
        {
            context = ArrayPool.Get<object>(parameters.Length);
            map = ArrayPool.Get<int?>(args.Length);
            int count = 0;

            for (int p = 0; p != parameters.Length; p++)
            {
                ParameterInfo parameter = parameters[p];
                Type baseType = parameter.ParameterType;

                if (baseType.IsByRef) baseType = baseType.GetElementType();

                if (TrySetFromArgument(parameter, baseType, args, map, context))
                {
                    count++;
                    continue;
                }

                if (parameter.IsOut)
                {
                    context[parameter.Position] = null;
                    continue;
                }

                if (parameter.DefaultValue != DBNull.Value)
                {
                    context[parameter.Position] = parameter.DefaultValue;
                    continue;
                }

                if (parameter.IsOptional)
                {
                    if (baseType.IsValueType)
                    {
                        context[parameter.Position] = Activator.CreateInstance(baseType);
                        continue;
                    }
                }

                object service = Interface.LiteLoader.ServiceProvider.GetService(baseType);

                if (service != null)
                {
                    context[parameter.Position] = service;
                    continue;
                }

                if (!parameter.IsOptional)
                {
                    count = -1;
                    ArrayPool.Free(context);
                    ArrayPool.Free(map);
                    context = null;
                    map = null;
                    break;
                }

                if (Nullable.GetUnderlyingType(baseType) != null)
                {
                    context[parameter.Position] = null;
                    continue;
                }

                context[parameter.Position] = Activator.CreateInstance(baseType);
            }

            return count;
        }

        private bool TrySetFromArgument(ParameterInfo parameter, Type parameterType, object[] arguments, int?[] map, object[] context)
        {
            for (int i = 0; i != arguments.Length; i++)
            {
                if (map[i].HasValue) continue;

                object arg = arguments[i];

                if (parameter.IsOut)
                {
                    if (arg != null) continue;
                    context[parameter.Position] = null;
                    map[i] = parameter.Position;
                    return true;
                }

                if (arg == null)
                {
                    if (parameter.ParameterType.IsByRef)
                    {
                        if (Nullable.GetUnderlyingType(parameterType) == null)
                        {
                            continue;
                        }

                        context[parameter.Position] = null;
                        map[i] = parameter.Position;
                        return true;
                    }

                    continue;
                }

                if (!parameterType.IsInstanceOfType(arg))
                {
                    if (TryBind(parameter, arg, out object bind) && parameterType.IsInstanceOfType(bind))
                    {
                        context[parameter.Position] = bind;
                        map[i] = parameter.Position;
                        return true;
                    }

                    continue;
                }

                context[parameter.Position] = arg;
                map[i] = parameter.Position;
                return true;
            }

            return false;
        }

        private bool TryBind(ParameterInfo parameter, object input, out object output)
        {
            lock (Subscriptions)
            {
                for (int i = 0; i != Subscriptions.Count; i++)
                {
                    IParameterBinding binding = Subscriptions[i];
                    if (!binding.TryBind(parameter, input, out output))
                    {
                        continue;
                    }

                    return true;
                }
                output = null;
                return false;
            }
        }

        public void ProcessReferences(ParameterInfo[] parameters, object[] givenArguments, object[] context, int?[] map)
        {
            for (int i = 0; i != map.Length; i++)
            {
                if (map[i] == null || !map[i].HasValue) continue;

                ParameterInfo parameter = parameters[map[i].Value];

                if (parameter.IsOut || parameter.ParameterType.IsByRef)
                {
                    givenArguments[i] = context[parameter.Position];
                }
            }
        }

#if !NET35

        private object HandleTaskReturn(System.Threading.Tasks.Task task, int startTime, int maxExecutionTime = 500)
        {
            Type taskType = task.GetType();
            bool hasGenericParameters = taskType.GetGenericArguments().Length > 0;

            if (task.Status == System.Threading.Tasks.TaskStatus.Created)
            {
                task.Start();
            }

            if (!hasGenericParameters)
            {
                return DBNull.Value;
            }

            if (!task.IsCompleted)
            {
                int waitTime = Environment.TickCount - startTime;
                if (waitTime < maxExecutionTime)
                {
                    task.Wait(maxExecutionTime - waitTime);
                }
            }

            if (task.Exception != null)
            {
                throw task.Exception;
            }

            if (task.IsCompleted)
            {
                PropertyInfo _taskResultProperty = taskType.GetProperty("Result", BindingFlags.Public | BindingFlags.Instance);
                object value = _taskResultProperty.GetValue(task, null);

                if (value != null && _taskType.IsInstanceOfType(value))
                {
                    return HandleTaskReturn((System.Threading.Tasks.Task)value, startTime, maxExecutionTime);
                }

                return value;
            }

            return hasGenericParameters ? null : DBNull.Value;
        }

#endif
    }
}
