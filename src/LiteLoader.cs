﻿using LiteLoader.DependencyInjection;
using LiteLoader.Extensions;
using LiteLoader.Logging;
using LiteLoader.Plugins;
using System;
using System.Linq;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace LiteLoader
{
    internal sealed class LiteLoader : ILiteLoader
    {
        private bool isLoaded = false;
        internal static bool startupComplete = false;
        private string _game;

        #region Services

        private IInvokeManager activationUtility;
        private ExtensionLoader extensionLoader;
        private RootPluginHandler pluginHandler;
        private DynamicServiceProvider serviceCollection;
        private ThreadedLogger logger;

        public IServiceProvider ServiceProvider => serviceCollection;

        public bool DisposeOnUnload(IDisposable disposable)
        {
            StackFrame frame = new StackFrame(1, false);
            return DisposeOnUnload(frame.GetMethod().DeclaringType.Assembly, disposable);
        }

        public bool DisposeOnUnload(Assembly assembly, IDisposable disposable)
        {
            return extensionLoader.DisposeOnUnload(assembly, disposable);
        }

        #endregion

        #region FileSystem

        public string ExtensionDirectory { get; }

        public string PluginDirectory { get; }

        public string LibraryDirectory { get; }

        public string LoaderDirectory { get; }
        public string LoggingDirectory { get; }
        public string RootDirectory { get; }

        public string GetExtensionPath(Assembly assembly)
        {
            if (assembly == null) return null;

            return extensionLoader.GetExtensionPath(assembly.GetName().Name);
        }

        #endregion

        public LiteLoader()
        {
            LibraryDirectory = GetType().Assembly.Location;
            RootDirectory = Environment.CurrentDirectory;

            if (RootDirectory.StartsWith(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)))
            {
                RootDirectory = AppDomain.CurrentDomain.BaseDirectory;
            }

            if (RootDirectory == null)
            {
                RootDirectory = LibraryDirectory;
            }

            LoaderDirectory = Path.Combine(RootDirectory, "LiteLoader");
            ExtensionDirectory = Path.Combine(LoaderDirectory, "Extensions");
            PluginDirectory = Path.Combine(LoaderDirectory, "Plugins");
            LoggingDirectory = Path.Combine(LoaderDirectory, "Logs");
        }

        public object InvokeHook(string name, object[] arguments)
        {
            if (string.IsNullOrEmpty(name))
            {
                return null;
            }

            return pluginHandler?.InvokeAll(name, arguments);
        }

#pragma warning disable IDE0051 // Remove unused private members

        private void Load(string game, DebugCallback debugCallback = null)
#pragma warning restore IDE0051 // Remove unused private members
        {
            if (isLoaded)
            {
                return;
            }
            isLoaded = true;
            _game = game;
            serviceCollection = new DynamicServiceProvider();
            activationUtility = new InvokeManager();
            serviceCollection.SetActivationContext(activationUtility);
            serviceCollection.Add(ServiceDescriptor.CreateSingleton<IInvokeManager, InvokeManager>((InvokeManager)activationUtility));
            serviceCollection.Add(ServiceDescriptor.CreateSingleton<ILogger, ThreadedLogger>(logger = new ThreadedLogger() { DebugCallback = debugCallback }));
            serviceCollection.Add(ServiceDescriptor.CreateSingleton<IPluginHandler, RootPluginHandler>(pluginHandler = new RootPluginHandler()));

            if (!Directory.Exists(LoaderDirectory))
            {
                logger.LogMessage($"Creating {nameof(LoaderDirectory)}", priority: LogMessage.Level.Debug);
                Directory.CreateDirectory(LoaderDirectory);
                logger.LogMessage($"Created {nameof(LoaderDirectory)} at {LoaderDirectory}", priority: LogMessage.Level.Information);
            }
            if (!Directory.Exists(LoggingDirectory))
            {
                logger.LogMessage($"Creating {nameof(LoggingDirectory)}", priority: LogMessage.Level.Debug);
                Directory.CreateDirectory(LoggingDirectory);
                logger.LogMessage($"Created {nameof(LoggingDirectory)} at {LoggingDirectory}", priority: LogMessage.Level.Information);
            }
            if (!Directory.Exists(ExtensionDirectory))
            {
                logger.LogMessage($"Creating {nameof(ExtensionDirectory)}", priority: LogMessage.Level.Debug);
                Directory.CreateDirectory(ExtensionDirectory);
                logger.LogMessage($"Created {nameof(ExtensionDirectory)} at {ExtensionDirectory}", priority: LogMessage.Level.Information);
            }
            if (!Directory.Exists(PluginDirectory))
            {
                logger.LogMessage($"Creating {nameof(PluginDirectory)}", priority: LogMessage.Level.Debug);
                Directory.CreateDirectory(PluginDirectory);
                logger.LogMessage($"Created {nameof(PluginDirectory)} at {PluginDirectory}", priority: LogMessage.Level.Information);
            }

            extensionLoader = new ExtensionLoader(activationUtility, ExtensionDirectory, pluginHandler = (RootPluginHandler)serviceCollection.GetService<IPluginHandler>(), logger);
            serviceCollection.Add(ServiceDescriptor.CreateSingleton<IExtensionLoader, ExtensionLoader>(extensionLoader));
            AppDomain.CurrentDomain.AssemblyResolve += DefaultResolver.Resolve;
            var result = extensionLoader.Load($"LiteLoader.{game}");
            if (result.Message != null)
            {
                logger.LogMessage(result.Message);
            }
            if (result.Exception != null)
            {
                logger.LogMessage(result.Exception);
            }
            extensionLoader.PreloadFileSystem();

            Game.ICommandHandler cmds = ServiceProvider.GetService<Game.ICommandHandler>();
            cmds?.RegisterCommand("liteloader.version", VersionCommand);
            cmds?.RegisterCommand("liteloader.loadextension", LoadExtensionCommand);
            cmds?.RegisterCommand("liteloader.unloadextension", UnloadExtensionCommand);
            cmds?.RegisterCommand("liteloader.loadplugin", LoadPluginCommand);
            cmds?.RegisterCommand("liteloader.unloadplugin", UnloadPluginCommand);
            cmds?.RegisterCommand("liteloader.plugins", PluginsCommand);
        }

#pragma warning disable IDE0051 // Remove unused private members

        private void Unload()
#pragma warning restore IDE0051 // Remove unused private members
        {
            if (!isLoaded)
            {
                return;
            }

            isLoaded = false;
            pluginHandler.UnloadAllPlugins();
            pluginHandler = null;
            extensionLoader = null;
            activationUtility = null;
            serviceCollection = null;
        }

        #region Commands

        private void VersionCommand(Game.ICommandContext context)
        {
            if (!context.Executor.IsAdmin)
            {
                context.Reply("No permission!");
                return;
            }

            string value = $"LiteLoader for {_game?.ToUpperInvariant()}{Environment.NewLine}" +
                $"Core Module: {GetType().Assembly.GetName().Name} ({GetType().Assembly.GetName().Version})";

            if (GetType().Assembly.GetCustomAttributes(false).OfType<DebuggableAttribute>().Any(a => a.IsJITTrackingEnabled))
            {
                value += " [Debug Build]";
            }

            if (extensionLoader.StartupExtension != null)
            {
                bool isDebugBuild = extensionLoader.StartupExtension.GetCustomAttributes(false).OfType<DebuggableAttribute>()
                    .Any(c => c.IsJITTrackingEnabled);

                value += $"{Environment.NewLine}Game Module: {extensionLoader.StartupExtension.GetName().Name} ({extensionLoader.StartupExtension.GetName().Version})";
                if (isDebugBuild) value += " [Debug Build]";
            }

            context.Reply(value);
        }

        private void LoadExtensionCommand(Game.ICommandContext context)
        {
            if (!context.Executor.IsAdmin)
            {
                context.Reply("No Permission!");
                return;
            }

            if (context.Arguments.Count() == 0)
            {
                context.Reply("Please specify a extension name. | i.e. liteloader.loadextension LiteLoader.MyExtension");
                return;
            }

            string extName = string.Join(string.Empty, context.Arguments.ToArray());
            var result = extensionLoader.Load(extName);

            if (result.Success)
            {
                context.Reply($"Successfully loaded extension {result.Extension.Title} v{result.Extension.Version} by {result.Extension.Author}");
                return;
            }

            string err = $"Failed to load {extName}";

            if (!string.IsNullOrEmpty(result.Message)) err += $" - {result.Message}";

            if (result.Exception != null)
            {
                err += $"{Environment.NewLine}{result.Exception.GetType().Name}: {result.Exception.Message}";
            }

            context.Reply(err);
        }

        private void UnloadExtensionCommand(Game.ICommandContext context)
        {
            if (!context.Executor.IsAdmin)
            {
                context.Reply("No Permission!");
                return;
            }

            if (context.Arguments.Count() == 0)
            {
                context.Reply("Please specify a extension name. | i.e. liteloader.unloadextension LiteLoader.MyExtension");
                return;
            }

            string extName = string.Join(string.Empty, context.Arguments.ToArray());
            var result = extensionLoader.Unload(extName);

            if (result.Success)
            {
                context.Reply($"Successfully unloaded extension {result.Extension.Title}");
                return;
            }

            string err = $"Failed to unload {extName}";

            if (!string.IsNullOrEmpty(result.Message)) err += $" - {result.Message}";

            if (result.Exception != null)
            {
                err += $"{Environment.NewLine}{result.Exception.GetType().Name}: {result.Exception.Message}";
            }

            context.Reply(err);
        }

        private void LoadPluginCommand(Game.ICommandContext context)
        {
            if (!context.Executor.IsAdmin)
            {
                context.Reply("No Permission!");
                return;
            }

            if (context.Arguments.Count() == 0)
            {
                context.Reply("Please specify a plugin name. | i.e. liteloader.loadplugin MyPlugin");
                return;
            }

            string pgnName = string.Join(string.Empty, context.Arguments.ToArray());
            var result = pluginHandler.LoadPlugin(pgnName);

            if (result.Success)
            {
                context.Reply($"Successfully loaded plugin {result.Plugin.Title} v{result.Plugin.Version} by {result.Plugin.Author}");
                return;
            }

            string err = $"Failed to load {pgnName}";

            if (!string.IsNullOrEmpty(result.Message)) err += $" - {result.Message}";

            if (result.Exception != null)
            {
                err += $"{Environment.NewLine}{result.Exception.GetType().Name}: {result.Exception.Message}";
            }

            context.Reply(err);
        }

        private void UnloadPluginCommand(Game.ICommandContext context)
        {
            if (!context.Executor.IsAdmin)
            {
                context.Reply("No Permission!");
                return;
            }

            if (context.Arguments.Count() == 0)
            {
                context.Reply("Please specify a plugin name. | i.e. liteloader.unloadplugin MyPlugin");
                return;
            }

            string pgnName = string.Join(string.Empty, context.Arguments.ToArray());
            var result = pluginHandler.LoadPlugin(pgnName);

            if (result.Success)
            {
                context.Reply($"Successfully unloaded plugin {result.Plugin.Title}");
                return;
            }

            string err = $"Failed to unload {pgnName}";

            if (!string.IsNullOrEmpty(result.Message)) err += $" - {result.Message}";

            if (result.Exception != null)
            {
                err += $"{Environment.NewLine}{result.Exception.GetType().Name}: {result.Exception.Message}";
            }

            context.Reply(err);
        }

        private void PluginsCommand(Game.ICommandContext context)
        {
            if (!context.Executor.IsAdmin)
            {
                context.Reply("No Permission!");
                return;
            }

            var pluginList = pluginHandler.GetPlugins();
            string msg = $"Showing {pluginList.Count()} loaded plugins{Environment.NewLine}";
            foreach (var pluginName in pluginList)
            {
                Plugin plugin = pluginHandler.GetPlugin(pluginName);
                msg += $"{plugin.Name} - {plugin.Title} v{plugin.Version.ToString(3)} by {plugin.Author}{Environment.NewLine}";
            }

            msg = msg.TrimEnd('\n', '\r');
            context.Reply(msg);
        }

        #endregion
    }
}
