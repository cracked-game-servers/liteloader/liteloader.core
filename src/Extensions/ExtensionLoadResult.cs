﻿using System;

namespace LiteLoader.Extensions
{
    internal class ExtensionLoadResult : IExtensionResult
    {
        public Exception Exception { get; }
        public IExtensionContainer Extension { get; }
        public string Message { get; }
        public bool Success { get; }

        private ExtensionLoadResult(bool success, string message, Exception exception, IExtensionContainer extension)
        {
            Success = success;
            Message = message;
            Exception = exception;
            Extension = extension;
        }

        public static IExtensionResult Fail(string message, Exception e) => new ExtensionLoadResult(false, message, e, null);

        public static IExtensionResult Fail(Exception e) => Fail(e.Message, e);

        public static IExtensionResult Fail(string message) => Fail(message, null);

        public static IExtensionResult Successful(IExtensionContainer container, string message) => new ExtensionLoadResult(true, message, null, container);

        public static IExtensionResult Successful(IExtensionContainer container) => Successful(container, null);
    }
}
