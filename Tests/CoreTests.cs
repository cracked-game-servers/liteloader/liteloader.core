using LiteLoader.DependencyInjection;
using LiteLoader.Extensions;
using LiteLoader.Logging;
using LiteLoader.Utilities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using Xunit;
using Xunit.Abstractions;

[assembly: ExtensionInfo(Startup = typeof(LiteLoader.Core.Tests.Startup))]

namespace LiteLoader.Core.Tests
{
    public static class Startup
    {
        public static void ConfigureServices(IDynamicServiceCollection services, ILogger logger, IExtensionLoader loader)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));
            if (logger == null) throw new ArgumentNullException(nameof(logger));
            services.AddSingleton<Game.IEngine, TestEngine>();

            var invoker = services.GetService<IInvokeManager>();
            invoker.Subscribe<DateTimeConverter>();
            logger.LogMessage($"Configure Services Called! | Engine: {services.GetService<Game.IEngine>().GetType().FullName}");
        }
    }

    public class CoreTests : IDisposable
    {
        public static ITestOutputHelper Output;
        private readonly Assembly CoreAssembly;
        private readonly string CoreBuildOutput;
        private readonly string CoreLibrary;
        private readonly string CoreSymbols;
        private readonly string SolutionLocation;
        private readonly string TestLocation;

        // Test Initialization
        public CoreTests(ITestOutputHelper outHelper)
        {
            Output = outHelper;
            TestLocation = Path.GetDirectoryName(GetType().Assembly.Location);

            SolutionLocation = Path.GetFullPath(Path.Combine(TestLocation, $"..{Path.DirectorySeparatorChar}..{Path.DirectorySeparatorChar}..{Path.DirectorySeparatorChar}..{Path.DirectorySeparatorChar}"));
            CoreBuildOutput = Path.Combine(SolutionLocation, "src", "bin");

            if (Directory.Exists(Path.Combine(CoreBuildOutput, "Debug")))
            {
                CoreBuildOutput = Path.Combine(CoreBuildOutput, "Debug", "netstandard20");
            }
            else if (Directory.Exists(Path.Combine(CoreBuildOutput, "Release")))
            {
                CoreBuildOutput = Path.Combine(CoreBuildOutput, "Release", "netstandard20");
            }
            else
            {
                throw new IOException("Unable to location Core Assembly");
            }

            CoreLibrary = Path.Combine(CoreBuildOutput, "LiteLoader.Core.dll");
            CoreSymbols = Path.Combine(CoreBuildOutput, "LiteLoader.Core.pdb");

            byte[] lib, symbs;

            if (File.Exists(CoreLibrary))
            {
                lib = File.ReadAllBytes(CoreLibrary);
            }
            else
            {
                throw new IOException($"Unable to locate " + CoreLibrary);
            }

            if (File.Exists(CoreSymbols))
            {
                symbs = File.ReadAllBytes(CoreSymbols);
            }
            else
            {
                symbs = null;
            }

            CoreAssembly = symbs != null ? Assembly.Load(lib, symbs) : Assembly.Load(lib);

            Interface.Initialize("TestsExtension", (lm) => WriteLine(lm.ToString(true)));
            LoadTestExtension();
        }

        // Test Cleanup
        public void Dispose()
        {
            Interface.Shutdown();
            Output = null;
        }

        #region Instance Checking

        [Fact, Trait("Category", "Instance Checks")]
        public void CheckLiteLoader()
        {
            Type expectedType = CoreAssembly.GetType("LiteLoader.LiteLoader", true, true);
            Assert.Equal(expectedType.FullName, Interface.LiteLoader.GetType().FullName);
        }

        #endregion

        #region Performance Tests

        public bool Executed = false;

        [Fact, Trait("Category", "Performance")]
        public void HookSpeed()
        {
            Interface.Invoke("TestInit", new Action(() => { Executed = true; }));
            Dictionary<string, double> times = new Dictionary<string, double>();

            times["BasicHook"] = CallHook("BasicHook");
            times["BasicAsyncHook"] = CallHook("BasicAsyncHook");
            times["ReturnHook"] = CallHook("ReturnHook", expectedReturn: "HelloWorld");
            times["ReturnAsyncHook"] = CallHook("ReturnAsyncHook", expectedReturn: "HelloWorldAsync");
            object value = new Object();
            times["ReturnParamHook"] = CallHook("ReturnParamHook", new object[] { value }, expectedReturn: value);
            times["ReturnAsyncParamHook"] = CallHook("ReturnAsyncParamHook", new object[] { value }, expectedReturn: value);
            DateTime now = DateTime.UtcNow;
            times["ParamConversionHook"] = CallHook("ParamConversionHook", new object[] { now }, expectedReturn: now.ToLongDateString());

            WriteLine("#### Hook Results ####");
            foreach (var time in times)
            {
                WriteLine($"Hook: {time.Key} | Speed: {time.Value}ms");
            }
        }

        private double CallHook(string hook, object[] parameters = null, int times = 500, object expectedReturn = null)
        {
            List<double> timings = new List<double>(times);
            Stopwatch timer = new Stopwatch();
            if (parameters == null) parameters = ArrayPool.Get<object>(0);

            for (int i = 0; i != times; i++)
            {
                Executed = false;
                timer.Start();
                object value = Interface.Invoke(hook, parameters);
                timer.Stop();
                int timeouts = 0;
                while (!Executed && timeouts < 5)
                {
                    System.Threading.Thread.Sleep(50);
                }
                Assert.True(Executed, $"Failed to execute Hook '{hook}'");
                Assert.Equal(expectedReturn, value);
                timings.Add(timer.Elapsed.TotalMilliseconds);
                timer.Reset();
            }

            WriteLine($"Executed Hook '{hook}' {times} times");
            return timings.Average();
        }

        #endregion

        private void LoadTestExtension()
        {
            IExtensionLoader extensionLoader = Interface.LiteLoader.ServiceProvider.GetService<IExtensionLoader>();
            Assert.NotNull(extensionLoader);
            var loadResult = extensionLoader.Load(File.ReadAllBytes(GetType().Assembly.Location), null);
            Assert.NotNull(loadResult);
            Assert.True(loadResult.Success, $"Extension failed to load: {loadResult.Message} - {loadResult.Exception}");
        }

        private void WriteLine(object message = null, string tag = null)
        {
            if (message == null)
            {
                Output.WriteLine(string.Empty);
                return;
            }
            string msg = tag != null ? $"[{tag}] " + message : message.ToString();
            Output.WriteLine(msg);
        }
    }

    public class DateTimeConverter : IParameterBinding
    {
        public bool TryBind(ParameterInfo parameter, object inputValue, out object outputValue)
        {
            if (parameter.Name.Equals("longDate", StringComparison.Ordinal) && parameter.ParameterType == typeof(string) && inputValue is DateTime date)
            {
                outputValue = date.ToLongDateString();
                return true;
            }
            outputValue = null;
            return false;
        }
    }
}
