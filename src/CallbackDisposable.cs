﻿using System;

namespace LiteLoader
{
    internal class CallbackDisposable : IDisposable
    {
        private readonly Action OnDispose;
        private bool ran = false;

        public CallbackDisposable(Action onDispose)
        {
            OnDispose = onDispose;
        }

        ~CallbackDisposable()
        {
            Dispose(true);
        }

        public void Dispose() => Dispose(false);

        private void Dispose(bool finalized)
        {
            if (ran)
            {
                return;
            }

            ran = true;

            if (!finalized)
            {
                GC.SuppressFinalize(this);
            }

            try
            {
                OnDispose();
            }
            catch (ObjectDisposedException)
            {
            }
        }
    }
}
